<?php
require_once 'requete.php'; 

echo "********** Bienvenue sur l'application Pokemon **********\n";

$type = array();
$language;

do {

    displayPokemonType();

    $type_number = trim(fgets(STDIN));

    switch($type_number){
        case 1 :
            $type []= "normal";
            break;
        case 2 :
            $type []= "fighting";
            break;
        case 3 :
            $type []= "flying";
            break;
        case 4 :
            $type []= "poison";
            break;
        case 5 :
            $type []= "ground";
            break;
        case 6  :
            $type []= "rock";
            break;
        case 7  :
            $type []= "bug";
            break;
        case 8  :
            $type []= "ghost";
            break;
        case 9  :
            $type []= "steel";
            break;
        case 10 :
            $type []= "fire";
            break;
        case 11 :
            $type []= "water";
            break;
        case 12 :
            $type []= "grass";
            break;
        case 13 :
            $type []= "electric";
            break;
        case 14 :
            $type []= "psychic";
            break;
        case 15 :
            $type []= "ice";
            break;
        case 16 :
            $type []= "dragon";
            break;
        case 17 :
            $type []= "dark";
            break;
        case 18 :
            $type []= "fairy";
            break;
        case 19 :
            $type []= "unknown";
            break;
        case 20 :
            $type []= "shadow";
            break;
        case 21 :
            echo "recherche des résultats\n";
            break;
        default :
            echo "saisie non valide\n";
    }

} while ($type_number != 21);


do {

    displayLanguage();

    $language_number = trim(fgets(STDIN));

    switch($language_number){
        case 1 :
            $language = "fra";
            break;
        case 2 :
            $language = "eng";
            break;
        default:
            echo "saisie non valide\n";
    }


} while ($language_number != 1 && $language_number != 2);

printPokemon($type, $language);


/**
 * Affiche les types de pokémon disponible 
 **/
function displayPokemonType(){

    echo "Veuillez choisir un type de pokemon : \n";
    echo "1  - normal\n";
    echo "2  - fighting\n";
    echo "3  - flying\n";
    echo "4  - poison\n";
    echo "5  - ground\n";
    echo "6  - rock\n";
    echo "7  - bug\n";
    echo "8  - ghost\n";
    echo "9  - steel\n";
    echo "10 - fire\n";
    echo "11 - water\n";
    echo "12 - grass\n";
    echo "13 - electric\n";
    echo "14 - psychic\n";
    echo "15 - ice\n";
    echo "16 - dragon\n";
    echo "17 - dark\n";
    echo "18 - fairy\n";
    echo "19 - unknown\n";
    echo "20 - shadow\n";
    echo "21 - choisir la langue de la recherche\n";
    echo "********** Entrer un nombre de 1 à 21 **********\n";
}


/**
 * Affiche les langues disponible 
 **/
function displayLanguage(){

    echo "Veuillez choisir une langue : \n";
    echo "1  - français\n";
    echo "2  - anglais\n";
    echo "********** Entrer un nombre de 1 à 2 **********\n";
}
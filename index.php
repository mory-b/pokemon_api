<?php require_once 'requete.php'; ?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Rest API Pokemon</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"/>
    </head>
    <body>
        <div class="root">

            <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-5">
                <a class="navbar-brand" href="#">Pokemon</a>
            </nav>

            <div class="container">

                <!-- formulaire de recherche des pokémons par type et choix de la langue -->
                <form method="get" class="mb-5">
                    <div class="form-group">
                        <label for="type_of_pokemon">Type de pokémon / Type of pokemon</label>
                        <select class="form-control" id="type_of_pokemon" name="type_of_pokemon[]" multiple required>
                            <?php 
                                printTypeOfPokemon();
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="language">Choisir une langue / Choose language</label>
                        <select class="form-control" id="language" name="language">
                            <option value="fra">Français / French</option>
                            <option value="eng">Anglais / English</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary"> Recherche </button>
                </form>

                <?php 
                    if (isset($_GET['type_of_pokemon'])) {
                    
                        //Affichage des types sélectionnés
                        printTypeSearch($_GET['type_of_pokemon'], $_GET['language']); 

                        // affichage des résultats de la recherche
                        displayPokemon($_GET['type_of_pokemon'], $_GET['language']);
                    }
                ?> 
            </div>

        </div>
    </body>
</html>


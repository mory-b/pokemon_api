<?php

require 'vendor/autoload.php';

use GuzzleHttp\Client;


/**
 * récupère les différents type de pokemon
 * */
function fetchTypeOfPokemon () 
{
    $types_pokemon = json_cached_api_results('type/', false);
    $types_pokemon_array = array();

    foreach ($types_pokemon->results as $value) {
        array_push($types_pokemon_array, $value->name) ;
    }

    return $types_pokemon_array;
}


/**
 * Affiche les différents type de pokemon
 **/
function printTypeOfPokemon()
{

    $types_array = fetchTypeOfPokemon();

    foreach ($types_array as $value) {
        echo "<option value='$value'> $value </option>" ;
    }
}


/**
 * affiche le(s) type(s) de pokemon(s) choisi(en)t par l'utilisateur
 **/
function printTypeSearch($types, $language) 
{

    $type = ($language == 'fra') ?  'Type(s) recherché(s)': 'Type of research ';
    echo ' <h3 class="text-center mb-5"> '. $type.' : '. implode(" - ", $types).'</h3>';
}


/**
 * Affiche les statistiques d'un pokemon sur le web
 **/
function statsWeb($stats)
{

    $statistics = '';  

    foreach (array_reverse($stats) as $values) {
        $name = $values["name"];
        $value = $values["stat"];
        $statistics .= "<div class='col col-md-6'> $name : $value  </div>";
    }

    return $statistics;
}


/**
 * Affiche les statistiques d'un pokemon en CLI
 **/
function statsCLI($stats)
{

    $statistics = '';  

    foreach (array_reverse($stats) as $values) {
        $name = $values["name"];
        $value = $values["stat"];
        $statistics .= "$name : $value; ";
    }

    echo "$statistics\n";
}


/**
 * Affiche les différentes versions du jeu ou apparait un pokemon
 **/
function printGames($versions, $cli = false)
{

    $games = implode(", ",array_reverse($versions));

    if($cli){
        echo "$games\n";
    }else {
        return $games;
    }
}


/**
 * Crée un tableau de pokemon en fonction des critères de l'utilisateur
 **/
function fetchPokemon($types, $language) 
{

        $type_pokemons = json_cached_api_results('type/'.$types[0], false);

        $pokemon_array = array();
        $store_games_versions = array();
        $store_stats = array();
        $nb_pokemon = 0;

        foreach ($type_pokemons->pokemon as $pokemon) {

            if ($nb_pokemon == 50){
                break;
            } 
            
            $le_pokemon = json_cached_api_results($pokemon->pokemon->url, true);
        
            if (count($types) > 1 ) {
                if( count($types) > count($le_pokemon->types)){
                    continue;
                } else {
                    $pokemon_type = array();

                    foreach ($le_pokemon->types as $value) {
                        array_push($pokemon_type,$value->type->name);
                    }

                    foreach($types as $type){
                        if(!in_array($type, $pokemon_type)){
                            continue 2;
                        }
                    }
                }
            }

            $games = array();
            $stats = array();
            $image = $le_pokemon->sprites->front_default;
                
            $species = json_cached_api_results($le_pokemon->species->url, true);
            $name = $language === "fra" ? $species->names[6]->name : $species->names[2]->name;

            foreach ($le_pokemon->game_indices as $game) {

                    $la_version = json_cached_api_results($game->version->url, true);
                    
                    $games[] = ($language === "fra" ) ? $la_version->names[1]->name : $la_version->names[4]->name;
            }

            foreach ($le_pokemon->stats as $stat) {

                    $la_stat = json_cached_api_results($stat->stat->url, true);
                    
                    $stats [] = ['stat' => $stat->base_stat, 'name' => ($language === "fra" ) ? $la_stat->names[1]->name : $la_stat->names[5]->name]; 
            }

            $pokemon_array[] = [
                'image' => $image,
                'games' => $games,
                'stats' => $stats,
                'name'  => $name,
            ];

            $nb_pokemon++;     
        }

        return $pokemon_array;
}


/**
 * affiche les pokemon sur le web
 **/
function displayPokemon($types, $language)
{

    $pokemons = fetchPokemon($types, $language);

    $statistics_title =  ($language === "fra" ) ? 'Statistiques' : 'Statistics';
    $games_title =  ($language === "fra" ) ? 'Versions' : 'Games';

    echo '<div class="row">';
    
    foreach ($pokemons as $value) {

        echo '
            <div class="col col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-4 mb-2">
                <div class="card">
                    <img src="'. $value['image'] .'" class="card-img-top" style="background-size: contain; background-color: beige;"/>
                    <div class="card-body">
                        <h5 class="card-title text-center font-weight-bold">'. $value['name'].'</h5>
                        <p class="card-text">
                            <div >
                                <h6 class="font-weight-bold">'. $statistics_title .'</h6>
                                <div class="row card_text" style="font-size: 13px">
                                    '. statsWeb($value['stats']) .'
                                </div>
                            </div>
                            <div >
                                <h6 class="mt-2 font-weight-bold">'. $games_title .'</h6>
                                <div style="font-size: 13px">
                                    '. printGames($value['games']) .'
                                </div>
                            </div>
                        </p>
                    </div>
                </div>
            </div>
        ';

    }

    echo '</div>';
}


/**
 * affiche les pokemons en CLI
 **/
function printPokemon($types, $language)
{

    $pokemons = fetchPokemon($types, $language);

    $statistics_title =  ($language === "fra" ) ? 'Statistiques' : 'Statistics';
    $games_title =  ($language === "fra" ) ? 'Versions' : 'Games';

    foreach ($pokemons as $value) {

        $name = $value['name'];

        echo "$name \n";

        echo "$statistics_title \n";

        statsCLI($value['stats']);

        echo "$games_title \n";

        printGames($value['games'], true);

        echo "\n";

        
    }
}


/**
 * requête et reponse de l'API
 **/
function fetchApiResult($search, $get_methode = false)
{

    $client = new Client(['base_uri' => 'https://pokeapi.co/api/v2/']);

    $response;

    if ($get_methode) {
        $response = $client->get($search);
    } else {
        $response = $client->request('GET', $search);
    }

    return $response->getBody();
}


/**
 * Mise en cache des réponses de l'API
 **/
function json_cached_api_results($search, $get_methode, $expires = NULL) 
{

    if (!$expires) {
        $expires = time() - 2*60*60;
    }

    $file_name = base64_encode($search);

    $cache_file = cacheFolder($file_name);

    if (!file_exists($cache_file)) {
        fopen($cache_file, "w");
    }

    if (filectime($cache_file) < $expires || file_get_contents($cache_file)  == '') {

        $json_results = fetchApiResult($search, $get_methode);

        if ( $json_results ) {
            file_put_contents($cache_file, $json_results);
        } else {
            unlink($cache_file);
        }
            
    } else {
        $json_results = file_get_contents($cache_file);
    }

    return json_decode($json_results);
}


/**
 * Crée le chemin du fichier à mettre en cache
 **/
function cacheFolder($file) {

    $path = dirname(__FILE__).'/cache/'. $file .'.json';
    return $path;
}
